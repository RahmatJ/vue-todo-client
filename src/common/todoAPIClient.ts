import { GetAllTodosAPIResponsePayload } from '@/interface/todoAPIClient.interface'
import axios, { AxiosInstance, AxiosRequestConfig } from 'axios'

class TodoAPIClient {
  baseURL = 'localhost:8000'
  headers = {
    'Access-Control-Allow-Origin': '*'
  }
  private apiInstance: AxiosInstance

  constructor() {
    this.apiInstance = axios.create({
      baseURL: this.baseURL
    })
  }

  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  private apiCall = async <T>(config: AxiosRequestConfig) => {
    try {
      const { data } = await this.apiInstance.request(config)
      return { data }
    } catch (error) {
      return { error }
    }
  }

  createTodos = async (name: string) => {
    return this.apiCall<GetAllTodosAPIResponsePayload>({
      method: 'post',
      url: '/todos',
      data: name,
      headers: {}
    })
  }

  getAllTodos = async () => {
    return this.apiCall<GetAllTodosAPIResponsePayload>({
      method: 'get',
      url: '/todos'
    })
  }

  getTodoById = async (id: number) => {
    return this.apiCall<TodoAPIClient>({
      method: 'get',
      url: `/todos/${id}`
    })
  }

  updateTodoById = async (id: number, name: string) => {
    return this.apiCall<TodoAPIClient>({
      method: 'patch',
      url: `/todos/${id}`,
      data: name
    })
  }

  deleteTodoById = async (id: number) => {
    return this.apiCall<TodoAPIClient>({
      method: 'delete',
      url: `/todos/${id}`
    })
  }
}

const todoAPI = new TodoAPIClient()
export default todoAPI
