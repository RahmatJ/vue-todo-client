export default interface TodoItem {
  id: number;
  name: string;
  date: string;
}
