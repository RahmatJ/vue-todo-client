import TodoItem from "./TodoItem";

interface TodoAPIResponsePayload {
  error?: unknown;
}

export interface GetAllTodosAPIResponsePayload extends TodoAPIResponsePayload {
  data?: TodoItem[];
}

export interface TodoDataAPIResponsePayload extends TodoAPIResponsePayload {
  data?: TodoItem;
}
